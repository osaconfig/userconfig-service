package main

import (
	"errors"
	"log"

	"github.com/davecgh/go-spew/spew"
	usercfgPb "gitlab.com/osaconfig/protobufs/userconfig"
)

func attachGroupBinds(bindList *[]string, providerNetwork *usercfgPb.GlobalOverrides_ProviderNetwork) error {
	for x := range *bindList {
		groupBind, ok := usercfgPb.GroupBind_value[(*bindList)[x]]
		if !ok {
			return errors.New("Invalid Group Bind")
		}
		log.Printf(spew.Sdump(groupBind))
		providerNetwork.GroupBinds = append(providerNetwork.GroupBinds,
			usercfgPb.GroupBind(groupBind))
	}
	return nil
}

func getContainerBridge(bridgeName string) usercfgPb.ContainerBridge {
	gb, ok := usercfgPb.ContainerBridge_value[bridgeName]
	if !ok {
		log.Fatalf("Invalid Group Bind: %s - %v", bridgeName, gb)
	}
	return usercfgPb.ContainerBridge(usercfgPb.ContainerBridge_value[bridgeName])
}

func getAnchorBlock(anchorBlock string) usercfgPb.AnchorBlock {
	return usercfgPb.AnchorBlock(usercfgPb.AnchorBlock_value[anchorBlock])
}

func groupBindName(groupBind int32) (string, error) {
	gbStr, ok := usercfgPb.GroupBind_name[groupBind]
	if !ok {
		return "", errors.New("Invalid Group Bind")
	}
	return gbStr, nil
}
