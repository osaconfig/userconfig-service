// inventory-service/datastore.go
package main

import (
	"fmt"
	"log"
	"os"

	"github.com/davecgh/go-spew/spew"
	mgo "gopkg.in/mgo.v2"
)

type dbConnect struct {
	user string
	pass string
	host string
}

func (db *dbConnect) String() string {
	return fmt.Sprintf("mongodb://%v:%v@%v", db.user, db.pass, db.host)
}

// CreateSession creates the main session to our datastore
func CreateSession(host string) (*mgo.Session, error) {
	var url dbConnect
	url.user = os.Getenv("DB_USER")
	url.pass = os.Getenv("DB_PASS")
	url.host = host
	log.Print(spew.Sdump(url.String()))
	session, err := mgo.Dial(url.String())
	if err != nil {
		return nil, err
	}

	session.SetMode(mgo.Monotonic, true)

	return session, nil
}
