// userconfig-service/handler.go
package main

import (
	"context"

	usercfgPb "gitlab.com/osaconfig/protobufs/userconfig"
	mgo "gopkg.in/mgo.v2"
)

// Our gRPC service handler
type service struct {
	session *mgo.Session
}

func (s *service) GetRepo() Repository {
	return &UserConfigRepository{s.session.Clone()}
}

func (s *service) GetUserConfig(ctx context.Context, req *usercfgPb.GetRequest, res *usercfgPb.Response) error {
	defer s.GetRepo().Close()
	// Find the environment
	ucfg, err := s.GetRepo().GetUserConfig(req)
	if err != nil {
		return err
	}

	// Set the inventory as part of the response message type
	res.UserConfig = ucfg
	return nil
}

func (s *service) CreateUserConfig(ctx context.Context, req *usercfgPb.UserConfig, res *usercfgPb.Response) error {
	defer s.GetRepo().Close()
	if err := s.GetRepo().CreateUserConfig(req); err != nil {
		return err
	}
	res.UserConfig = req
	res.Created = true
	return nil
}

func (s *service) RemoveUserConfig(ctx context.Context, req *usercfgPb.UserConfig, res *usercfgPb.Response) error {
	defer s.GetRepo().Close()
	if err := s.GetRepo().RemoveUserConfig(req); err != nil {
		return err
	}
	res.UserConfig = req
	res.Removed = true
	return nil
}
