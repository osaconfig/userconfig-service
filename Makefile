UNAME_S := $(shell uname -s)
SEDCMD := sed -i ''      # MacOS sed need a place holeder for the inplace edit backup file
ifeq ($(UNAME_S),Linux)  # Linux does not need the placeholder and errors out if it's there
	SEDCMD = sed -i
endif

build:
	docker build -t registry.gitlab.com/osaconfig/userconfig-service:latest .
	docker push registry.gitlab.com/osaconfig/userconfig-service:latest

run:
	docker run -d \
		-p 8080 \
		-e MICRO_SERVER_ADDRESS=:8080 \
		-e MICRO_REGISTRY=mdns \
		registry.gitlab.com/osaconfig/userconfig-service:latest

deploy-minikube:
	sed "s/{{ DB_PASSWORD }}/$(shell kubectl get secret --namespace osaconfig datastore-mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)/g" ./deployments/deployment.tmpl > ./deployments/deployment.yml
	$(SEDCMD) "s/{{ UPDATED_AT }}/$(shell date)/g" ./deployments/deployment.yml
	kubectl replace -f ./deployments/deployment.yml
