// userconfig-service/main.go
package main

import (
	"fmt"
	"log"
	"os"

	micro "github.com/micro/go-micro"
	k8s "github.com/micro/kubernetes/go/micro"
	usercfgPb "gitlab.com/osaconfig/protobufs/userconfig"
)

const (
	defaultHost = "localhost:27017"
)

func createDummyData(repo Repository) {
	defer repo.Close()

	// CIDR Networks and Used IPs
	cidr := []*usercfgPb.CIDRNetwork{
		{Id: "container", CidrNetwork: "10.0.236.0", CidrNetmask: "22"},
		{Id: "lbaas", CidrNetwork: "10.0.232.0", CidrNetmask: "22"},
		{Id: "tunnel", CidrNetwork: "10.0.240.0", CidrNetmask: "22"},
		{Id: "storage", CidrNetwork: "10.0.244.0", CidrNetmask: "22"},
		{Id: "flat", CidrNetwork: "10.0.248.0", CidrNetmask: "22"},
	}

	usedIps := []*usercfgPb.UsedIPs{
		{Id: "range001", UsedipsStart: "10.0.232.0", UsedipsEnd: "10.0.232.200"},
		{Id: "range002", UsedipsStart: "10.0.236.0", UsedipsEnd: "10.0.236.200"},
		{Id: "range003", UsedipsStart: "10.0.240.0", UsedipsEnd: "10.0.240.200"},
		{Id: "range004", UsedipsStart: "10.0.244.0", UsedipsEnd: "10.0.244.200"},
		{Id: "range005", UsedipsStart: "10.0.248.0", UsedipsEnd: "10.0.248.200"},
	}

	// Provider Networks
	var providerNetworks []*usercfgPb.GlobalOverrides_ProviderNetwork

	// Management Network
	mgmtGroupBinds := []string{"ALLCONTAINERS", "HOSTS"}
	mgmtProvider := usercfgPb.GlobalOverrides_ProviderNetwork{
		ContainerBridge:    getContainerBridge("BRMGMT"),
		ContainerType:      "veth",
		ContainerInterface: "eth1",
		IpFromQ:            "container",
		Type:               "raw",
		IsContainerAddress: true,
		IsSshAddress:       true,
	}
	attachGroupBinds(&mgmtGroupBinds, &mgmtProvider)
	providerNetworks = append(providerNetworks, &mgmtProvider)

	// OVN Optional Networks
	// TODO(d34dh0r53): This should only be inserted if OVN is enabled (Feature Flag Testing)
	neutronGroupBinds := []string{"NEUTRONLINUXBRIDGEAGENT"}
	neutronVXLANProvider := usercfgPb.GlobalOverrides_ProviderNetwork{
		ContainerBridge:    getContainerBridge("BRVXLAN"),
		ContainerType:      "veth",
		ContainerInterface: "eth10",
		IpFromQ:            "tunnel",
		Type:               "vxlan",
		Range:              "1:1000",
		NetName:            "vxlan",
	}
	attachGroupBinds(&neutronGroupBinds, &neutronVXLANProvider)
	providerNetworks = append(providerNetworks, &neutronVXLANProvider)

	vlanGroupBinds := append(neutronGroupBinds, "UTILITYALL")
	vlanNetwork := usercfgPb.GlobalOverrides_ProviderNetwork{
		ContainerBridge:    getContainerBridge("BRVLAN"),
		ContainerType:      "veth",
		ContainerInterface: "eth11",
		Type:               "vlan",
		Range:              "1:1",
		NetName:            "vlan",
	}
	attachGroupBinds(&vlanGroupBinds, &vlanNetwork)
	providerNetworks = append(providerNetworks, &vlanNetwork)

	flatNetwork := usercfgPb.GlobalOverrides_ProviderNetwork{
		ContainerBridge:    getContainerBridge("BRFLAT"),
		ContainerType:      "veth",
		ContainerInterface: "eth12",
		HostBindOverride:   "veth2",
		Type:               "flat",
		NetName:            "flat",
	}
	attachGroupBinds(&vlanGroupBinds, &flatNetwork)
	providerNetworks = append(providerNetworks, &flatNetwork)
	// End of ovn optional networks

	storageGroupBinds := []string{"GLANCEAPI", "CINDERAPI", "CINDERVOLUME", "NOVACOMPUTE", "SWIFTPROXY"}
	storageNetwork := usercfgPb.GlobalOverrides_ProviderNetwork{
		ContainerBridge:    getContainerBridge("BRSTORAGE"),
		ContainerType:      "veth",
		ContainerInterface: "eth2",
		IpFromQ:            "storage",
		Type:               "raw",
	}
	attachGroupBinds(&storageGroupBinds, &storageNetwork)
	providerNetworks = append(providerNetworks, &storageNetwork)

	lbaasGroupBinds := []string{"NEUTRONLINUXBRIDGEAGENT", "OCTAVIAWORKER", "OCTAVIAHOUSEKEEPING", "OCTAVIAHEALTHMONITOR"}
	lbassNetwork := usercfgPb.GlobalOverrides_ProviderNetwork{
		ContainerBridge:    getContainerBridge("BRLBAAS"),
		ContainerType:      "veth",
		ContainerInterface: "eth13",
		IpFromQ:            "lbaas",
		Type:               "flat",
		NetName:            "lbaas",
	}
	attachGroupBinds(&lbaasGroupBinds, &lbassNetwork)
	providerNetworks = append(providerNetworks, &lbassNetwork)

	swiftDrives := []*usercfgPb.GlobalOverrides_SwiftGlobalOverrides_SwiftDrive{
		{Name: "disk1"},
		{Name: "disk2"},
		{Name: "disk3"},
	}

	storagePolicies := []*usercfgPb.GlobalOverrides_SwiftGlobalOverrides_StoragePolicy{{
		Name:    "default",
		Index:   0,
		Default: true},
	}

	swiftGlobalOverrides := []*usercfgPb.GlobalOverrides_SwiftGlobalOverrides{{
		PartPower:          "8",
		StorageNetwork:     getContainerBridge("BRSTORAGE"),
		ReplicationNetwork: getContainerBridge("BRSTORAGE"),
		Drives:             swiftDrives,
		MountPoint:         "/srv",
		StoragePolicies:    storagePolicies,
	}}

	anchorBlocks := []*usercfgPb.Anchor{}

	cinder1ContainerVars := []*usercfgPb.ContainerVars{{
		ContainerTech: "lxc",
		StorageBackend: &usercfgPb.ContainerVars_StorageBackend{
			CinderBackend: &usercfgPb.ContainerVars_StorageBackend_Cinder{
				LimitContainerTypes: "cinder_volume",
				Lvm: &usercfgPb.ContainerVars_StorageBackend_Cinder_LVM{
					VolumeGroup:       "cinder-volumes",
					VolumeDriver:      "cinder.volume.drivers.lvm.LVMVolumeDriver",
					VolumeBackendName: "LVM_iSCSI",
					IscsiIpAddress:    "10.0.244.130",
				},
			},
		},
	}}

	cinder2ContainerVars := []*usercfgPb.ContainerVars{{
		ContainerTech: "lxc",
		StorageBackend: &usercfgPb.ContainerVars_StorageBackend{
			CinderBackend: &usercfgPb.ContainerVars_StorageBackend_Cinder{
				LimitContainerTypes: "cinder_volume",
				Lvm: &usercfgPb.ContainerVars_StorageBackend_Cinder_LVM{
					VolumeGroup:       "cinder-volumes",
					VolumeDriver:      "cinder.volume.drivers.lvm.LVMVolumeDriver",
					VolumeBackendName: "LVM_iSCSI",
					IscsiIpAddress:    "10.0.244.131",
				},
			},
		},
	}}
	cinderAnchorBlock := &usercfgPb.Anchor{
		AnchorName: getAnchorBlock("CINDER"),
		Host: []*usercfgPb.Anchor_Host{
			{HostName: "cinder1", Ip: "10.0.244.130", ContainerVars: cinder1ContainerVars},
			{HostName: "cinder2", Ip: "10.0.244.131", ContainerVars: cinder2ContainerVars},
		},
	}
	anchorBlocks = append(anchorBlocks, cinderAnchorBlock)

	containerVars := []*usercfgPb.ContainerVars{{
		ContainerTech: "lxc",
	}}
	computeAnchorBlock := &usercfgPb.Anchor{
		AnchorName: getAnchorBlock("COMPUTE"),
		Host: []*usercfgPb.Anchor_Host{
			{HostName: "compute1", Ip: "10.0.236.120", ContainerVars: containerVars},
			{HostName: "compute2", Ip: "10.0.236.121", ContainerVars: containerVars},
		},
	}
	anchorBlocks = append(anchorBlocks, computeAnchorBlock)

	infraAnchorBlock := &usercfgPb.Anchor{
		AnchorName: getAnchorBlock("INFRA"),
		Host: []*usercfgPb.Anchor_Host{
			{HostName: "infra1", Ip: "10.0.236.100", ContainerVars: containerVars},
			{HostName: "infra2", Ip: "10.0.236.101", ContainerVars: containerVars},
			{HostName: "infra3", Ip: "10.0.236.102", ContainerVars: containerVars},
		},
	}
	anchorBlocks = append(anchorBlocks, infraAnchorBlock)

	swiftAnchorBlock := &usercfgPb.Anchor{
		AnchorName: getAnchorBlock("SWIFT"),
		Host: []*usercfgPb.Anchor_Host{
			{HostName: "swift1", Ip: "10.0.236.140", ContainerVars: containerVars},
			{HostName: "swift2", Ip: "10.0.236.141", ContainerVars: containerVars},
			{HostName: "swift3", Ip: "10.0.236.142", ContainerVars: containerVars},
		},
	}
	anchorBlocks = append(anchorBlocks, swiftAnchorBlock)

	loadbalancerAnchorBlock := &usercfgPb.Anchor{
		AnchorName: getAnchorBlock("LOADBALANCER"),
		Host: []*usercfgPb.Anchor_Host{
			{HostName: "loadbalancer1", Ip: "10.0.236.150", ContainerVars: containerVars},
		},
	}
	anchorBlocks = append(anchorBlocks, loadbalancerAnchorBlock)

	logAnchorBlock := &usercfgPb.Anchor{
		AnchorName: getAnchorBlock("LOG"),
		Host: []*usercfgPb.Anchor_Host{
			{HostName: "logging1", Ip: "10.0.236.110", ContainerVars: containerVars},
		},
	}
	anchorBlocks = append(anchorBlocks, logAnchorBlock)

	globalOverrides := []*usercfgPb.GlobalOverrides{{
		InternalLbVipAddress: "10.0.236.150",
		ExternalLbVipAddress: "10.0.2.150",
		TunnelBridge:         getContainerBridge("BRVXLAN"),
		ManagementBridge:     getContainerBridge("BRMGMT"),
		ProviderNetworks:     providerNetworks,
		SwiftGlobalOverrides: swiftGlobalOverrides,
	}}

	userConfig := []*usercfgPb.UserConfig{{
		//		Environment:     "d58313a2-a02f-4cd8-8ce8-771acbbcfb69", // From customer-service
		Environment:     "testing-1", // From customer-service
		Description:     "lon3",      // From customer-service
		CidrNetworks:    cidr,
		UsedIps:         usedIps,
		GlobalOverrides: globalOverrides[0],
		AnchorBlocks:    anchorBlocks,
	}}

	for _, v := range userConfig {
		repo.RemoveUserConfig(v)
		repo.CreateUserConfig(v)
	}
}

func main() {

	// Database host from environment variables set in Dockerfile or spawning process
	host := os.Getenv("DB_HOST")
	// Fallback on defaultHost
	if host == "" {
		host = defaultHost
	}

	session, err := CreateSession(host)

	defer session.Close()

	if err != nil {
		log.Panicf("Could not connect to datastore with host %s - %v", host, err)
	}

	srv := k8s.NewService(
		micro.Name("osaconfig.userconfigservice"),
		micro.Version("latest"),
	)

	repo := &UserConfigRepository{session.Copy()}

	createDummyData(repo)

	srv.Init()

	// Register the handler
	usercfgPb.RegisterUserConfigServiceHandler(srv.Server(), &service{session})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
