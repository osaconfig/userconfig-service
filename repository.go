// userconfig-service/repository.go
package main

import (
	usercfgPb "gitlab.com/osaconfig/protobufs/userconfig"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dbName               = "osaconfig"
	userConfigCollection = "userconfigs"
)

// Repository - interface for user config repository methods
type Repository interface {
	GetUserConfig(*usercfgPb.GetRequest) (*usercfgPb.UserConfig, error)
	CreateUserConfig(*usercfgPb.UserConfig) error
	RemoveUserConfig(*usercfgPb.UserConfig) error
	Close()
}

// UserConfigRepository - struct to contain datastore session
type UserConfigRepository struct {
	session *mgo.Session
}

// GetUserConfig - returns an user config which consists of networks and used IPs
func (repo *UserConfigRepository) GetUserConfig(environ *usercfgPb.GetRequest) (*usercfgPb.UserConfig, error) {
	var userConfig *usercfgPb.UserConfig

	err := repo.collection().Find(bson.M{
		"environment": bson.M{"$eq": environ.Environment},
	}).One(&userConfig)

	if err != nil {
		return nil, err
	}

	return userConfig, nil
}

// CreateUserConfig - creates a new user config in the datastore
func (repo *UserConfigRepository) CreateUserConfig(userConfig *usercfgPb.UserConfig) error {
	return repo.collection().Insert(userConfig)
}

// RemoveUserConfig - removes an userconfig from the collection
func (repo *UserConfigRepository) RemoveUserConfig(userConfig *usercfgPb.UserConfig) error {
	return repo.collection().Remove(bson.M{
		"environment": bson.M{"$eq": userConfig.Environment},
	})
}

// Close - close the datastore
func (repo *UserConfigRepository) Close() {
	repo.session.Close()
}

func (repo *UserConfigRepository) collection() *mgo.Collection {
	return repo.session.DB(dbName).C(userConfigCollection)
}
